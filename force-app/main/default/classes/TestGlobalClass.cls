global class TestGlobalClass {
    global static void staticMethod(){
        System.debug('static method');
        System.debug('Managed yml method');
    }
    global void nonStaticMethod(){
        System.debug('instance method');
    }
}